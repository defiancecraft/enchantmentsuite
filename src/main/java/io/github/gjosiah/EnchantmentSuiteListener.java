package io.github.gjosiah;

import io.github.gjosiah.menu.EnchantmentMenu;

import org.bukkit.Bukkit;
import org.bukkit.Material;
import org.bukkit.block.Block;
import org.bukkit.event.EventHandler;
import org.bukkit.event.Listener;
import org.bukkit.event.block.Action;
import org.bukkit.event.player.PlayerInteractEvent;
import org.bukkit.material.MaterialData;

public class EnchantmentSuiteListener implements Listener {

    public static MaterialData def = new MaterialData(Material.BOOKSHELF);
    
    private final MaterialData active;
    private final EnchantmentMenu menu;
    
    @SuppressWarnings("deprecation")
    public EnchantmentSuiteListener(EnchantmentMenu menu, EnchantmentSuiteConfig.ActiveBlock block) {
        MaterialData data = block.toMaterialData();
        this.active = data == null ? def : data;
        this.menu = menu;
        Bukkit.getLogger().info("[EnchantmentSuite] Using " + active.getItemType().name() + ":" + active.getData() + " for interaction.");
    }
    
    @EventHandler
    @SuppressWarnings("deprecation")
    public void onPlayerInteract(PlayerInteractEvent e) {
        if (e.getPlayer().isSneaking() || !Action.RIGHT_CLICK_BLOCK.equals(e.getAction())) {
            return;
        }
        
        Block clicked = e.getClickedBlock();
        
        if (clicked == null) {
            return;
        }
        
        if (active.getItemType().equals(clicked.getType())) {
            if (active.getData() < 0 || active.getData() == clicked.getData()) {
                menu.openShop(e.getPlayer());
            }
        }
    }
}
