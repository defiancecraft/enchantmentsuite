package io.github.gjosiah;

import java.util.Arrays;
import java.util.Iterator;
import java.util.Objects;

import org.bukkit.Material;
import org.bukkit.enchantments.Enchantment;
import org.bukkit.entity.Player;
import org.bukkit.inventory.ItemStack;
import org.bukkit.material.MaterialData;

import com.google.common.collect.Iterators;

public class EnchantmentSuiteConfig implements Iterable<EnchantmentSuiteConfig.EnchantmentSpecification> {

    private ActiveBlock block = new ActiveBlock();
    private EnchantmentInventory gui = new EnchantmentInventory();
    private EnchantmentSpecification[] enchantments = new EnchantmentSpecification[0];
    
    public EnchantmentSuiteConfig(ActiveBlock block, EnchantmentSpecification... configured) {
        this.block = block;
        this.enchantments = configured;
    }
    
    public EnchantmentSuiteConfig() {
        this(new ActiveBlock(), new EnchantmentSpecification[0]);
    }
    
    public Iterator<EnchantmentSpecification> iterator() {
        return enchantments == null ? Iterators.emptyIterator() : Iterators.forArray(enchantments);
    }
    
    public ActiveBlock getActiveBlock() {
        return block;
    }
    
    public EnchantmentInventory getInventorySettings() {
        return gui;
    }
    
    public EnchantmentSpecification[] getSpecifications() {
        return enchantments == null ? new EnchantmentSpecification[0] : enchantments;
    }
    
    public EnchantmentSpecification getSpecification(String name) {
        Objects.requireNonNull(name, "Cannot match an EnchantmentSpecification for a null name!");
        
        if (enchantments == null) {
            return null;
        }
        
        for (EnchantmentSpecification espec : enchantments) {
            if (name.equals(espec.name)) {
                return espec;
            }
        }
        
        return null;
    }
    
    
    public static class ActiveBlock {
    
        private Material material;
        private byte data;
        
        private transient MaterialData materialdata;
        
        public ActiveBlock(Material material, byte data) {
            this.material = material;
            this.data = data;
        }
        
        public ActiveBlock() {
            this(null, (byte) -1);
        }
        
        @SuppressWarnings("deprecation")
        public MaterialData toMaterialData() {
            if (materialdata == null && material != null) {
                this.materialdata = new MaterialData(material, data);
            }
            
            return materialdata;
        }
        
        @SuppressWarnings("deprecation")
        public boolean matches(MaterialData data) {
            return matches(data.getItemType(), data.getData());
        }
        
        public boolean matches(Material material, byte data) {
            return this.material == material && this.data == data;
        }
    }
    
    public static class EnchantmentInventory {
    
        private String title;
        private boolean checkvalid;
        private ActiveBlock filler;
        private ActiveBlock confirm;
        private ActiveBlock back;
        
        public EnchantmentInventory() {
            this.title = "Enchantment Suite";
            this.checkvalid = true;
            this.filler = new ActiveBlock(Material.IRON_FENCE, (byte) 0);
            this.confirm = new ActiveBlock(Material.WOOL, (byte) 5);
            this.back = new ActiveBlock(Material.WOOL, (byte) 14);
        }
        
        public String getTitle() {
            return title == null ? "Enchantment Suite" : title;
        }
        
        public boolean isCheckingValidity() {
            return checkvalid;
        }
        
        public MaterialData getFiller() {
            return filler == null ? null : filler.toMaterialData();
        }
        
        public MaterialData getConfirm() {
            return confirm == null ? null : confirm.toMaterialData();
        }
        
        public MaterialData getBack() {
            return back == null ? null : back.toMaterialData();
        }
    }
    
    /**
     * A Class for representing a configured Enchantment's specification.
     */
    public static class EnchantmentSpecification {
    
        private String name;
        private String message;
        private EnchantmentTier[] tiers;
        
        private transient boolean sorted;
        private transient Enchantment enchant;
        
        public EnchantmentSpecification(String name, String message, EnchantmentTier... tiers) {
            this.name = name;
            this.message = message;
            this.tiers = tiers;
            this.sorted = false;
        }
        
        public EnchantmentSpecification() {
            this(null, null, new EnchantmentTier[0]);
        }
        
        public String getLockedMessage() {
            return message == null ? "" : message;
        }
        
        public Enchantment getEnchantment() {
            if (enchant == null || !enchant.getName().equals(name)) {
                if (name == null) {
                    return null;
                }
                
                this.enchant = Enchantment.getByName(name);
            }
            
            return enchant;
        }
        
        private EnchantmentTier[] sort() {
            if (tiers == null) {
                return new EnchantmentTier[0];
            }
            
            if (!sorted) {
                Arrays.sort(tiers, (t1, t2) -> {
                    return t2.maxlevel - t1.maxlevel;
                });
                
                this.sorted = true;
            }
            
            return tiers;
        }
        
        public EnchantmentTier getTier(Player player) {
            for (EnchantmentTier etier : sort()) {
                if (etier.canEnchant(player)) {
                    return etier;
                }
            }
            
            return null;
        }
        
        public boolean canEnchant(Player player) {
            EnchantmentTier etier = getTier(player);
            
            return etier == null ? false : etier.canEnchant(player);
        }
        
        public boolean canEnchant(Player player, int level) {
            EnchantmentTier etier = getTier(player);
            
            return etier == null ? false : etier.canEnchant(level) && etier.canAfford(player, level);
        }
    }
    
    public static class EnchantmentTier {
    
        private int maxlevel;
        private String permission;
        private double[] costs;
        
        public EnchantmentTier(int maxlevel, String permission, double[] costs) {
            this.maxlevel = maxlevel;
            this.permission = permission;
            this.costs = costs;
        }
        
        public EnchantmentTier() {
            this(-1, null, new double[0]);
        }
        
        public int getMaxLevel() {
            return maxlevel;
        }
        
        public void process(Player player, Enchantment enchantment, int level, ItemStack apply) {
            if (canEnchant(player, level)) {
                if (EnchantmentSuite.getInstance().processCost(player, costs[level - 1])) {
                    apply.addUnsafeEnchantment(enchantment, level);
                }
            }
        }
        
        public boolean canEnchant(Player player, int level) {
            return canEnchant(level) && canEnchant(player) && canAfford(player, level);
        }
        
        public boolean canEnchant(int level) {
            return maxlevel >= level;
        }
        
        public boolean canEnchant(Player player) {
            return permission == null || EnchantmentSuite.getInstance().hasPermission(player, permission);
        }
        
        public double getCost(int level) {
            if (costs == null || (level - 1) >= costs.length) {
                return Double.NaN;
            }
            
            return costs[level - 1];
        }
        
        public boolean canAfford(Player player, int level) {
            if (costs == null || (level - 1) >= costs.length) {
                return false;
            }
            
            return EnchantmentSuite.getInstance().canAfford(player, costs[level - 1]);
        }
    }
}
