package io.github.gjosiah.util;

import org.bukkit.Material;
import org.bukkit.entity.Player;
import org.bukkit.inventory.ItemStack;
import org.bukkit.material.MaterialData;

import com.gmail.favorlock.commonutils.ui.MenuItem;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;
import java.util.function.Consumer;
import java.util.function.Supplier;

/**
 * An interface for denoting an Object that can be interpreted as both an
 * ItemStack and a MenuItem.
 * 
 * @author Gideon
 */
public interface MenuItemAdapter extends Consumer<Player>, Supplier<ItemStack> {

    public String getDisplay();
    
    public String[] getLore();
    
    public int getQuantity();
    
    public Material getMaterial();
    
    public Byte getData();
    
    public Short getDurability();
    
    public void onClick(Player player);
    
    @SuppressWarnings("deprecation")
    public default MaterialData getMaterialData() {
        Material material = getMaterial();
        Byte data = getData();
        
        if (material == null) {
            throw new IllegalArgumentException("The Material cannot be null!");
        }
        
        if (data == null) {
            return new MaterialData(material);
        } else {
            return new MaterialData(material, data);
        }
    }
    
    public default List<String> getLoreList() {
        String[] lore = getLore();
        
        return lore == null ? new ArrayList<>() : Arrays.asList(lore);
    }
    
    public default ItemStack get() {
        return getMaterialData().toItemStack(getQuantity());
    }
    
    public default void accept(Player player) {
        onClick(player);
    }
    
    public default MenuItemWrapper toMenuItem() {
        return new MenuItemWrapper(this);
    }
    
    
    public static class MenuItemWrapper extends MenuItem {
    
        protected final MenuItemAdapter item;
        
        protected MenuItemWrapper(MenuItemAdapter item) {
            super(item.getDisplay(), item.getMaterialData(), item.getQuantity(), asDurabilityFor(item));
            
            this.item = item;
            this.setData(asDurabilityFor(item));
            this.setDescriptions(item.getLoreList());
        }
        
        public void onClick(Player player) {
            item.onClick(player);
        }
        
        public void onClick(Player player, String information) {
            onClick(player);
        }
        
        
        public static short asDurabilityFor(MenuItemAdapter item) {
            Short dura = item.getDurability();
            Byte data = item.getData();
            
            if (dura != null) {
                return dura.shortValue();
            } else if (data != null) {
                return data.shortValue();
            } else {
                return 0;
            }
        }
    }
}
