package io.github.gjosiah.util;

import java.util.function.BiPredicate;
import java.util.function.Consumer;

import org.bukkit.entity.Player;
import org.bukkit.event.inventory.InventoryClickEvent;
import org.bukkit.inventory.ItemStack;
import org.bukkit.material.MaterialData;

import com.gmail.favorlock.commonutils.ui.MenuItem;

public class SimpleMenuItem extends MenuItem {

    private final Consumer<? super Player> click;
    private final BiPredicate<? super Player, InventoryClickEvent> eventClick;
    
    public SimpleMenuItem(MaterialData data, String display) {
        super(display, data, 1);
        
        this.click = null;
        this.eventClick = null;
    }
    
    public SimpleMenuItem(ItemStack item, String display, Consumer<? super Player> click) {
        super(display, item.getData(), item.getAmount());
        
        this.click = click;
        this.eventClick = null;
    }
    
    public SimpleMenuItem(ItemStack item, String display, BiPredicate<? super Player, InventoryClickEvent> click) {
        super(display, item.getData(), item.getAmount());
        
        this.eventClick = click;
        this.click = null;
    }
    
    public SimpleMenuItem(ItemStack item, String display) {
        super(display, item.getData(), item.getAmount());
        
        this.click = null;
        this.eventClick = null;
    }
    
    public SimpleMenuItem(ItemStack item) {
        this(item, item.getItemMeta().getDisplayName());
    }
    
    public void onClick(Player player) {
        if (click != null) {
            click.accept(player);
        }
    }
    
    public boolean onClick(Player player, InventoryClickEvent e) {
        if (eventClick != null) {
            return eventClick.test(player, e);
        } else {
            onClick(player);
            return false;
        }
    }
}
