package io.github.gjosiah.menu;

import io.github.gjosiah.EnchantmentSuiteConfig.EnchantmentSpecification;
import io.github.gjosiah.EnchantmentSuiteConfig.EnchantmentTier;

import org.bukkit.ChatColor;
import org.bukkit.Material;
import org.bukkit.entity.Player;

class LevelBook extends ShopItemAdapter {

    private static final String DISPLAY_FORMAT = String.format("%s%%s%s Level %s%%s", ChatColor.GOLD, ChatColor.GRAY, ChatColor.GOLD);
    private static final Material material = Material.ENCHANTED_BOOK;
    
    private final EnchantmentSpecification espec;
    private final int level;
    private final EnchantmentShop shop;
    
    LevelBook(EnchantmentSpecification espec, int level, EnchantmentShop shop) {
        super(material, null, level, p -> String.format(DISPLAY_FORMAT, espec.getEnchantment().getName(), level), p -> {
            EnchantmentTier etier = espec.getTier(p);
            String[] messages;
            
            if (etier != null && espec.canEnchant(p, level)) {
                messages = new String[] { "Cost: " + etier.getCost(level), "Click to use."};
            } else if (etier != null && !etier.canEnchant(p)) {
                messages = new String[] { "Cost: " + etier.getCost(level), espec.getLockedMessage() };
            } else if (etier != null && !etier.canAfford(p, level)) {
                messages = new String[] { "Cost: " + etier.getCost(level), "You cannot afford this enchantment." };
            } else {
                messages = new String[] { etier == null ? "" : String.valueOf(etier.getCost(level)), espec.getLockedMessage() };
            }
            
            return messages;
        });
        
        this.espec = espec;
        this.level = level;
        this.shop = shop;
    }
    
    public void onClick(Player player) {
        if (espec == null || !espec.canEnchant(player, level)) {
            player.sendMessage(espec == null ? "You cannot use this enchantment." : espec.getLockedMessage());
            return;
        } else {
            shop.chooseLevel(level, player);
        }
    }
}
