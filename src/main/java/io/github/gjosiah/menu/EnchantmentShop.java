package io.github.gjosiah.menu;

import io.github.gjosiah.EnchantmentSuite;
import io.github.gjosiah.EnchantmentSuiteConfig.EnchantmentSpecification;
import io.github.gjosiah.EnchantmentSuiteConfig.EnchantmentTier;
import io.github.gjosiah.util.ItemStackUtil;
import io.github.gjosiah.util.SimpleMenuItem;

import org.bukkit.Bukkit;
import org.bukkit.ChatColor;
import org.bukkit.Material;
import org.bukkit.entity.Player;
import org.bukkit.event.inventory.InventoryClickEvent;
import org.bukkit.inventory.ItemStack;
import org.bukkit.material.MaterialData;

import com.gmail.favorlock.commonutils.ui.Menu;
import com.gmail.favorlock.commonutils.ui.MenuItem;

public class EnchantmentShop extends Menu {

    private final ItemStack confirm;
    private final ItemStack back;
    private final EnchantmentMenu menu;
    private final boolean check;
    private ShopState state;
    private EnchantmentSpecification category;
    private int level;
    
    @SuppressWarnings("deprecation")
    EnchantmentShop(EnchantmentMenu menu, String title, int rows) {
        super(title, rows);
        
        this.menu = menu;
        
        MaterialData c = menu.config.getInventorySettings().getConfirm(), b = menu.config.getInventorySettings().getBack();
        this.confirm = c == null ? new MaterialData(Material.WOOL, (byte) 5).toItemStack() : c.toItemStack();
        this.back = b == null ? new MaterialData(Material.WOOL, (byte) 14).toItemStack() : b.toItemStack();
        
        this.check = menu.config.getInventorySettings().isCheckingValidity();
        this.state = ShopState.UNINITIALIZED;
        this.category = null;
        this.level = -1;
        
        super.exitOnClickOutside = false;
        super.menuCloseBehavior = player -> {
            this.returnItem(player);
            this.destroy();
        };
    }
    
    public void returnItem(Player player) {
        ItemStack item = getInventory().getItem(EnchantmentMenu.ENCHANT_ITEM_SLOT);
        
        if (item != null) {
            player.getWorld().dropItem(player.getEyeLocation(), item);
            getInventory().setItem(EnchantmentMenu.ENCHANT_ITEM_SLOT, new ItemStack(Material.AIR));
        }
    }
    
    void init(MenuItem[] items, Player player) {
        super.items = items;
        super.inventory = Bukkit.createInventory(this, 9 * getRows(), getTitle());
        
        for (int i = 0; i < items.length; i++) {
            MenuItem item = items[i];
            
            if (item == null) {
                continue;
            }
            
            getInventory().setItem(i, item instanceof ShopItem ? ((ShopItem) item).getItemStack(player) : item.getItemStack());
        }
        
        addMenuItem(new SimpleMenuItem(confirm, "Confirm", this::confirm), EnchantmentMenu.CONFIRM_SLOT);
        addMenuItem(new SimpleMenuItem(back, "Back", this::back), EnchantmentMenu.BACK_SLOT);
        addMenuItem(new SimpleMenuItem(new ItemStack(Material.AIR), "item", this::acceptItem), EnchantmentMenu.ENCHANT_ITEM_SLOT);
        
        this.state = ShopState.ENCHANT_SELECTION;
        
        setupCategories(player);
        
        openMenu(player);
    }
    
    private void clearEnchantSlots() {
        for (int slot : EnchantmentMenu.ENCHANT_SLOTS) {
            removeMenuItem(slot);
        }
        
        getInventory().setItem(EnchantmentMenu.ENCHANT_CHOOSE_SLOT, menu.filler.getItemStack());
    }
    
    private void setupCategories(Player player) {
        clearEnchantSlots();
        
        ShopItem[] categories = menu.getEnchantmentCategories(this);
        
        for (int i = 0; i < categories.length; i++) {
            int slot = EnchantmentMenu.ENCHANT_SLOTS[i];
            items[slot] = categories[i];
            getInventory().setItem(slot, categories[i].getItemStack(player));
        }
    }
    
    private void setupLevels(Player player) {
        if (category != null) {
            clearEnchantSlots();
            
            ShopItem[] levels = menu.getEnchantmentLevels(this, category, player);
            
            for (int i = 0; i < levels.length; i++) {
                int slot = EnchantmentMenu.ENCHANT_SLOTS[i];
                items[slot] = levels[i];
                getInventory().setItem(slot, levels[i].getItemStack(player));
            }
        } else {
            this.state = ShopState.ENCHANT_SELECTION;
            setupCategories(player);
        }
    }
    
    void chooseCategory(EnchantmentSpecification espec, Player player) {
        if (ShopState.ENCHANT_SELECTION.equals(state)) {
            this.category = espec;
            this.state = ShopState.LEVEL_SELECTION;
            setupLevels(player);
        }
    }
    
    void chooseLevel(int level, Player player) {
        if (ShopState.LEVEL_SELECTION.equals(state) && category != null) {
            this.level = level;
            this.state = ShopState.AWAITING_CONFIRMATION;
            String display = ChatColor.GRAY + category.getEnchantment().getName() + " " + ChatColor.GOLD + level;
            String lore = ChatColor.GRAY + "Cost " + ChatColor.GOLD + category.getTier(player).getCost(level);
            
            getInventory().setItem(EnchantmentMenu.ENCHANT_CHOOSE_SLOT, ItemStackUtil.create(Material.PAPER, display, lore));
        }
    }
    
    /*
     * Returns true if the given click event should not be cancelled,
     * and the item should be put into the enchant item slot.
     */
    private boolean acceptItem(Player player, InventoryClickEvent e) {
        Bukkit.getScheduler().runTaskLater(EnchantmentSuite.getInstance(), player::updateInventory, 1L);
        
        if (ShopState.AWAITING_CONFIRMATION.equals(state)) {
            ItemStack cursor = e.getCursor();
            
            if (cursor == null || Material.AIR == cursor.getType()) {
                if (e.getCurrentItem() == null || e.getCurrentItem().getType() == Material.AIR) {
                    player.sendMessage("Place an item in this slot to enchant it!");
                    return false;
                } else {
                    return true;
                }
            } else if(!check || category.getEnchantment().canEnchantItem(cursor)) {
                if (cursor.containsEnchantment(category.getEnchantment())) {
                    player.sendMessage("That item already has that enchantment!");
                    return false;
                }
                
                return true;
            } else {
                player.sendMessage("You cannot apply that enchantment to that item!");
                return false;
            }
        } else {
            player.sendMessage("Please choose an enchantment and a level first!");
            return false;
        }
    }
    
    private void confirm(Player player) {
        switch (state) {
        case AWAITING_CONFIRMATION:
            ItemStack item = getInventory().getItem(EnchantmentMenu.ENCHANT_ITEM_SLOT);
            
            if (item != null && (!check || category.getEnchantment().canEnchantItem(item))) {
                EnchantmentTier etier = category.getTier(player);
                
                etier.process(player, category.getEnchantment(), level, item);
            } else {
                player.sendMessage("Please insert an item into the enchantment slot!");
            }
            
            break;
        case LEVEL_SELECTION:
            player.sendMessage("Please choose an enchantment level!");
            break;
        case ENCHANT_SELECTION:
            player.sendMessage("Please choose an enchantment category!");
            break;
        default:
            break;
        }
    }
    
    private void back(Player player) {
        switch (state) {
        case AWAITING_CONFIRMATION:
            removeMenuItem(EnchantmentMenu.ENCHANT_CHOOSE_SLOT);
            addMenuItem(menu.filler, EnchantmentMenu.ENCHANT_CHOOSE_SLOT);
            returnItem(player);
            
            this.level = -1;
            this.state = ShopState.LEVEL_SELECTION;
            
            setupLevels(player);
            
            break;
        case LEVEL_SELECTION:
            this.category = null;
            this.level = -1;
            this.state = ShopState.ENCHANT_SELECTION;
            
            setupCategories(player);
            
            break;
        case ENCHANT_SELECTION:
            player.closeInventory();
            break;
        default:
            break;
        }
    }
    
    
    private static enum ShopState {
    
        UNINITIALIZED,
        ENCHANT_SELECTION,
        LEVEL_SELECTION,
        AWAITING_CONFIRMATION;
    }
}
