package io.github.gjosiah.menu;

import io.github.gjosiah.EnchantmentSuiteConfig.EnchantmentSpecification;

import org.bukkit.ChatColor;
import org.bukkit.Material;
import org.bukkit.entity.Player;

class CategoryBook extends ShopItemAdapter {

    private static final String DISPLAY_FORMAT = String.format("%sEnchantment %s%%s", ChatColor.GRAY, ChatColor.GOLD);
    
    private final EnchantmentSpecification espec;
    private final EnchantmentShop shop;
    
    CategoryBook(EnchantmentSpecification espec, EnchantmentShop shop) {
        super(Material.ENCHANTED_BOOK, 0, 1, pd -> CategoryBook.getDisplay(espec, pd), pl -> CategoryBook.getLore(espec, pl));
        
        this.espec = espec;
        this.shop = shop;
    }
    
    public void onClick(Player player) {
        if (shop != null) {
            shop.chooseCategory(espec, player);
        }
    }
    
    
    private static String getDisplay(EnchantmentSpecification espec, Player p) {
        return String.format(DISPLAY_FORMAT, espec.getEnchantment().getName());
    }
    
    private static String[] getLore(EnchantmentSpecification espec, Player p) {
        String message = "";
        
        if (espec == null) {
            message = "[Locked]";
        } else {
            if (espec.canEnchant(p)) {
                message = "Click to select category.";
            } else {
                message = espec.getLockedMessage();
            }
        }
        
        return new String[] { message };
    }
}
