package io.github.gjosiah.menu;

import io.github.gjosiah.EnchantmentSuite;
import io.github.gjosiah.EnchantmentSuiteConfig;
import io.github.gjosiah.EnchantmentSuiteConfig.EnchantmentSpecification;
import io.github.gjosiah.EnchantmentSuiteConfig.EnchantmentTier;
import io.github.gjosiah.util.SimpleMenuItem;

import org.bukkit.Material;
import org.bukkit.entity.Player;
import org.bukkit.material.MaterialData;

import com.gmail.favorlock.commonutils.ui.MenuItem;

public class EnchantmentMenu {

//    static final MenuItem FILLER = new SimpleMenuItem(new ItemStack(Material.IRON_FENCE), " ");
//    static final String TITLE = "Enchantment Suite";
    static final int ROWS = 5;
    static final int[] FILLER_SLOTS = {
         0,  1,  2,  3,  4,  5,  6,  7,  8,
         9,     11,                     17,
        18,                             26,
        27,     29,                     35,
        36, 37, 38, 39, 40, 41, 42, 43, 44
    };
    static final int[] ENCHANT_SLOTS = {
        12, 13, 14, 15, 16,
        21, 22, 23, 24, 25,
        30, 31, 32, 33, 34
    };
    static final int ENCHANT_ITEM_SLOT = 19;
    static final int ENCHANT_CHOOSE_SLOT = 20;
    static final int CONFIRM_SLOT = 10;
    static final int BACK_SLOT = 28;
    
    private final MenuItem[] items;
    final EnchantmentSuiteConfig config;
    final MenuItem filler;
    
    public EnchantmentMenu(EnchantmentSuite suite) {
        this.items = new MenuItem[9 * ROWS];
        this.config = suite.getConfiguration();
        MaterialData filler_data = config.getInventorySettings().getFiller();
        this.filler = new SimpleMenuItem(filler_data == null ? new MaterialData(Material.IRON_FENCE) : filler_data, " ");
        
        init(config);
    }
    
    private void init(EnchantmentSuiteConfig config) {
        /*
         *  Inventory 9 x 6
         * X X X X X X X X X
         * X O C E E E E E X
         * X O X E E E E E X
         * X O X E E E E E X
         * X X X X X X X X X
         * 
         * O - Interactive slots (back, place item, confirm)
         * X - Filler slot
         * E - Enchantment view
         * C - Chosen enchantment (looks like a filler slot when not chosen)
         */
        
        // Fill the filler slots
        for (int slot : FILLER_SLOTS) {
            items[slot] = filler;
        }
        
        // The chosen enchantment slot uses the filler item until something is chosen
        items[ENCHANT_CHOOSE_SLOT] = filler;
    }
    
    public void openShop(Player player) {
        new EnchantmentShop(this, config.getInventorySettings().getTitle(), ROWS).init(items.clone(), player);
    }
    
    ShopItem[] getEnchantmentCategories(EnchantmentShop shop) {
        EnchantmentSpecification[] especs = config.getSpecifications();
        ShopItem[] categories = new ShopItem[especs.length];
        
        for (int i = 0; i < categories.length; i++) {
            categories[i] = new ShopItem(new CategoryBook(especs[i], shop));
        }
        
        return categories;
    }
    
    ShopItem[] getEnchantmentLevels(EnchantmentShop shop, EnchantmentSpecification espec, Player player) {
        EnchantmentTier etier = espec.getTier(player);
        
        if (etier == null) {
            return new ShopItem[0];
        }
        
        ShopItem[] items = new ShopItem[etier.getMaxLevel()];
        
        for (int i = 1; i <= etier.getMaxLevel(); i++) {
            items[i - 1] = new ShopItem(new LevelBook(espec, i, shop));
        }
        
        return items;
    }
}
