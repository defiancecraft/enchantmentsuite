package io.github.gjosiah.menu;

import io.github.gjosiah.util.MenuItemAdapter;

import java.util.Arrays;
import java.util.function.Function;

import org.bukkit.Material;
import org.bukkit.entity.Player;
import org.bukkit.inventory.ItemStack;
import org.bukkit.inventory.meta.ItemMeta;

abstract class ShopItemAdapter implements MenuItemAdapter {

    private final Function<Player, String> getDisplay;
    private final Function<Player, String[]> getLore;
    private final Material material;
    private final Number data;
    private final int quantity;
    
    ShopItemAdapter(Material material, Number data, int quantity, Function<Player, String> getDisplay, Function<Player, String[]> getLore) {
        this.getDisplay = getDisplay;
        this.getLore = getLore;
        this.material = material;
        this.data = data;
        this.quantity = quantity;
    }
    
    @Deprecated
    public String getDisplay() {
        return "";
    }
    
    @Deprecated
    public String[] getLore() {
        return new String[0];
    }
    
    public Material getMaterial() {
        return material;
    }
    
    public Byte getData() {
        return data instanceof Byte ? data.byteValue() : null;
    }
    
    public Short getDurability() {
        return data instanceof Short ? data.shortValue() : null;
    }
    
    public int getQuantity() {
        return quantity;
    }
    
    public abstract void onClick(Player player);
    
    public ItemStack getItemStack(Player player) {
        ItemStack item = getMaterialData().toItemStack(quantity);
        ItemMeta meta = item.getItemMeta();
        meta.setDisplayName(getDisplay.apply(player));
        meta.setLore(Arrays.asList(getLore.apply(player)));
        item.setItemMeta(meta);
        return item;
    }
}
