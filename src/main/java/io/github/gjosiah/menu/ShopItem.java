package io.github.gjosiah.menu;

import io.github.gjosiah.util.MenuItemAdapter;

import java.util.function.Function;

import org.bukkit.entity.Player;
import org.bukkit.inventory.ItemStack;

public class ShopItem extends MenuItemAdapter.MenuItemWrapper {
    
    private final Function<Player, ItemStack> customize;
    
    ShopItem(ShopItemAdapter item) {
        super(item);
        
        this.customize = item::getItemStack;
    }
    
    public void onClick(Player player) {
        item.onClick(player);
    }
    
    public void onClick(Player player, String information) {
        onClick(player);
    }
    
    ItemStack getItemStack(Player player) {
        return customize.apply(player);
    }
}
