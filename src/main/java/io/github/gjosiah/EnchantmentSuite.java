package io.github.gjosiah;

import io.github.gjosiah.menu.EnchantmentMenu;

import java.io.File;
import java.io.FileReader;
import java.io.FileWriter;
import java.io.IOException;

import net.milkbowl.vault.economy.Economy;
import net.milkbowl.vault.economy.EconomyResponse;
import net.milkbowl.vault.permission.Permission;

import org.bukkit.Bukkit;
import org.bukkit.craftbukkit.libs.com.google.gson.Gson;
import org.bukkit.entity.Player;
import org.bukkit.plugin.RegisteredServiceProvider;
import org.bukkit.plugin.java.JavaPlugin;

import com.gmail.favorlock.commonutils.ui.MenuAPI;

public class EnchantmentSuite extends JavaPlugin {

    private static EnchantmentSuite instance;
    
    private final Gson gson;
    private final File config_file;
    private final EnchantmentSuiteConfig config;
    private final EnchantmentMenu menu;
    private final boolean enabled;
    private MenuAPI menus;
    private Permission permissions;
    private Economy economy;
    private EnchantmentSuiteListener listener;
    
    public EnchantmentSuite() {
        this.gson = new Gson();
        this.config_file = new File(getDataFolder(), "config.json");
        
        if (!config_file.exists()) {
            getDataFolder().mkdirs();
            
            try (FileWriter writer = new FileWriter(config_file)) {
                gson.toJson(new EnchantmentSuiteConfig(), writer);
            } catch (IOException e) {
                getLogger().warning("Unable to create default configuration file!");
            }
        }
        
        EnchantmentSuiteConfig c;
        
        try (FileReader reader = new FileReader(config_file)) {
            c = gson.fromJson(reader, EnchantmentSuiteConfig.class);
        } catch (Exception e) {
            c = null;
            getLogger().severe("Error loading config.json- plugin cannot function properly!");
            e.printStackTrace();
        }
        
        this.config = c;
        
        if (config != null) {
            this.menu = new EnchantmentMenu(this);
            this.enabled = true;
        } else {
            this.menu = null;
            this.enabled = false;
        }
    }
    
    public void onEnable() {
        if (!enabled) {
            getLogger().severe("Plugin was not properly enabled; will not load.");
            return;
        }
        
        this.menus = new MenuAPI(this);
        
        if (getServer().getPluginManager().getPlugin("Vault") != null) {
            RegisteredServiceProvider<Economy> economyRSP = getServer().getServicesManager().getRegistration(Economy.class);
            
            if (economyRSP != null) {
                this.economy = economyRSP.getProvider();
            } else {
                getLogger().severe("Could not hook Economy from Vault!");
            }
            
            RegisteredServiceProvider<Permission> permissionRSP = getServer().getServicesManager().getRegistration(Permission.class);
            
            if (permissionRSP != null) {
                permissions = permissionRSP.getProvider();
            } else {
                getLogger().severe("Could not hook Permissions from Vault!");
            }
        }  else {
            getLogger().warning("Could not find Vault!");
        }
        
        this.listener = new EnchantmentSuiteListener(menu, config.getActiveBlock());
        Bukkit.getPluginManager().registerEvents(listener, this);
        
        EnchantmentSuite.instance = this;
    }
    
    public void onDisable() {
        EnchantmentSuite.instance = null;
    }
    
    public EnchantmentSuiteConfig getConfiguration() {
        return config;
    }
    
    public EnchantmentMenu getEnchantmentMenu() {
        return menu;
    }
    
    public MenuAPI getMenuAPI() {
        return menus;
    }
    
    public boolean hasPermission(Player player, String permission) {
        if (permissions == null) {
            return player.hasPermission(permission);
        } else {
            return permissions.has(player, permission);
        }
    }
    
    public Permission getPermissions() {
        return permissions;
    }
    
    public boolean canAfford(Player player, double cost) {
        if (economy == null) {
            return player.isOp();
        } else {
            return economy.getBalance(player.getName()) >= cost;
        }
    }
    
    public boolean processCost(Player player, double cost) {
        if (economy == null) {
            return player.isOp();
        } else {
            EconomyResponse r = economy.withdrawPlayer(player.getName(), cost);
            return r != null && r.transactionSuccess();
        }
    }
    
    public Economy getEconomy() {
        return economy;
    }
    
    
    public static EnchantmentSuite getInstance() {
        return instance;
    }
}
